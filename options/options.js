/* toggle "ajouter un compte" */
// css pour #info display: none 
var toggle  = document.getElementById("add_account");
var div_info = document.getElementById("info");
toggle.addEventListener("click", function() {
  div_info.style.display = (div_info.dataset.toggled ^= 1) ? "block" : "none";
  // addCompte;
  });

/* initialise variables for "user" and co */
var inputUser = document.querySelector(".new-account input[name='user']");
var inputId = document.querySelector(".new-account input[name='id']");
var inputPass = document.querySelector(".new-account input[name='pass']");
var accountContainer = document.querySelector(".account-container");

var addBtn = document.getElementById("add_ok");
addBtn.addEventListener('click', addCompte);

function onError(error) {
  console.log('sms_options_error: ' + error);
}

/* Affichage, au démarrage de la page 'options' des éléments stockés précédemment */
initialize();

function initialize() {
  var gettingAllStorageItems = browser.storage.local.get(null);
  gettingAllStorageItems.then((results) => {
    var accountKeys = Object.keys(results);
    for (let accountKey of accountKeys) {
      var curValue = results[accountKey];
      // [0] id; [1] pass
      displayCompte(accountKey,curValue[0], curValue[1]);
    }
  }, onError);
}


/* Ajoute un compte (+validation) et lance le stockage */
function addCompte() {
  var accountUser = inputUser.value;
  var accountId = inputId.value;
  var accountPass = inputPass.value;
  var gettingItem = browser.storage.local.get(accountUser);
  gettingItem.then((result) => {
    var objTest = Object.keys(result);
    if(objTest.length < 1 && accountUser !== '' && accountId !== '' && accountPass !== '') {
      inputUser.value = '';
      inputId.value = '';
      inputPass.value = '';
      storeCompte(accountUser,accountId, accountPass);
      // console.log('add : ' + accountUser + ' -- ' + accountId + ' -- ' + accountPass);
    }
  }, onError);
}

/* stockage et affichage du nouveau compte */
function storeCompte(user, id, pass) {
  var storingCompte = browser.storage.local.set({ [user] : [id, pass] });
  storingCompte.then(() => {
    displayCompte(user,id, pass);
  }, onError);
}

/* Affichage d'un compte (in the account box) */
function displayCompte(user, id, pass) {
  /* création boîte, div .account */
  var account = document.createElement('div');
  var accountDisplay = document.createElement('div');
  var accountH = document.createElement('h2');
  var deleteBtn = document.createElement('button');

  account.setAttribute('class','account');
  accountDisplay.setAttribute('class', 'browser-style')
  /* on affiche seulement "user" en <h2> */
  accountH.textContent = user;
  accountH.setAttribute('title', 'clic pour modifier');
  deleteBtn.setAttribute('class','delete browser-style');
  deleteBtn.textContent = 'Supprimer le compte';

  accountDisplay.appendChild(accountH);
  accountDisplay.appendChild(deleteBtn);

  account.appendChild(accountDisplay);

  /* set up listener for the delete functionality */
  deleteBtn.addEventListener('click',(e) => {
    const evtTgt = e.target;
    evtTgt.parentNode.parentNode.parentNode.removeChild(evtTgt.parentNode.parentNode);
    browser.storage.local.remove(user);
  })

  /* create account edit box */
  var accountEdit = document.createElement('div');
  var accountUserEdit = document.createElement('input');
  var accountIdEdit = document.createElement('input');
  var accountPassEdit = document.createElement('input');

  var updateBtn = document.createElement('button');
  var cancelBtn = document.createElement('button');

  accountEdit.setAttribute('class', 'browser-style');
  accountPassEdit.setAttribute('type', 'password');
  updateBtn.setAttribute('class','update browser-style');
  updateBtn.textContent = 'Sauver';
  cancelBtn.setAttribute('class','cancel browser-style');
  cancelBtn.textContent = 'Annuler';

  accountEdit.appendChild(accountUserEdit);
  accountUserEdit.value = user;
  accountEdit.appendChild(accountIdEdit);
  accountIdEdit.value = id;
  accountEdit.appendChild(accountPassEdit);
  accountPassEdit.value = pass;
  accountEdit.appendChild(updateBtn);
  accountEdit.appendChild(cancelBtn);
  /* accroche du div pour edit à la div .account */
  account.appendChild(accountEdit);
  accountEdit.style.display = 'none';

  /* et on accroche tout ça à la div .account-container */
  accountContainer.appendChild(account);


  /* set up listeners for the update functionality */
  accountH.addEventListener('click',() => {
    accountDisplay.style.display = 'none';
    accountEdit.style.display = 'block';
  })

  cancelBtn.addEventListener('click',() => {
    accountDisplay.style.display = 'block';
    accountEdit.style.display = 'none';
    accountUserEdit.value = user;
    accountIdEdit.value = id;
    accountPassEdit.value = pass;
  })

  updateBtn.addEventListener('click',() => {
    if(accountUserEdit.value !== user || accountIdEdit.value !== id || accountPassEdit.value !== pass) {
      updateAccount(user,accountUserEdit.value,accountIdEdit.value, accountPassEdit.value);
      account.parentNode.removeChild(account);
    } 
  });
}


/* Mise à jour du compte (et affichage), pas de vérif sur les nouvelles infos */
function updateAccount(actualUser, newUser, newId, newPass) {
  var storingaccount = browser.storage.local.set({ [newUser] : [newId, newPass] });
  storingaccount.then(() => {
    if(actualUser !== newUser) {
      var removingaccount = browser.storage.local.remove(actualUser);
      removingaccount.then(() => {
        displayCompte(newUser, newId, newPass);
      }, onError);
    } else {
      displayCompte(newUser, newId, newPass);
    }
  }, onError);
}