/* variables pour l'url */
var userName = '';
var userID = '';
var userPass = '';

var currentTabUrl;
var BaseUrl = "https://smsapi.free-mobile.fr/sendmsg?user=";

var ErrorList = {
    200 : "Le SMS a été envoyé sur le mobile.",
    400 : "Un des paramètres obligatoires est manquant.",
    402 : "Trop de SMS ont été envoyés en trop peu de temps.",
    403 : "Le service n’est pas activé sur l’espace abonné, ou login(id) / clé(pass) incorrect.",
    500 : "Erreur côté serveur. Veuillez réessayez ultérieurement."
}

/* Pour la gestion de la "drop down list" */
var select = document.getElementById("selectUser");
var allOptions = [];


function onError(error) {
  console.log('sms_popup_error: ' + error);
}


/* initialisation de la drop down list */
fillDown();


/* remplissage de la liste avec les données stockées {user : [id, pass]} */
function fillDown() {  
    var gettingAllStorageItems = browser.storage.local.get(null);
    gettingAllStorageItems.then((results) => {
        var noteKeys = Object.keys(results);
        for (let noteKey of noteKeys) {
            var curValue = results[noteKey];
            var el = document.createElement("option");
            el.textContent = noteKey;
            // el.value = curValue; // [0] id; [1] pass
            select.appendChild(el);
            allOptions[noteKey] = curValue;
        }
        addAccountAdd();
    }, onError);
}


/* on ajoute ... à la fin du "select" */
function addAccountAdd() {
    // console.log("ajout");
    var el = document.createElement("option");
    el.textContent = "ajouter un compte";
    select.appendChild(el);
}


/* onChange sur la drop down-list */
select.addEventListener('change', function() {
    var index = select.selectedIndex;
    var dernierSelect = select.options.length - 1;
    // sélection d'un compte
    if (index > 0 && index !== dernierSelect){
        userName = select.options[index].text;
        userID = allOptions[userName][0];
        userPass = allOptions[userName][1];
    // ajout d'un compte
    } else if (index === dernierSelect) {
            var opening = browser.runtime.openOptionsPage();
            opening.then(() => {
                window.close();
            });
    } else {
        userName = '';
        userID = '';
        userPass = '';
    }
    // console.log('index:'+ index + ' user:' + userName + ' id:' + userID + ' pass:' + userPass);
})


function notify(userName, textContent) {
  var title = "SMS pour " + userName;
  chrome.notifications.create({
    "type": "basic",
    "iconUrl": chrome.extension.getURL("icons/sms-48.png"),
    "title": title,
    "message": textContent
  });
}


function urlConstruct(userID, userPass, msg){
    return BaseUrl + userID + "&pass=" + userPass + "&msg=" + encodeURIComponent(msg);
}


function makeRequestAndClose(url) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if (xhr.readyState === 4) {
            notify(userName, ErrorList[xhr.status]);
            } else {
                onError(xhr.statusText);
            }
    };
    xhr.onloadend = function(){
        window.close();
    }
    xhr.open("GET", url, true);
    xhr.onerror = function (e) {
        onError(xhr.statusText);
    };
    xhr.send(null);
}



function isNotReadyNotify(userID, userPass){
    if (userID == '' || userPass == ''){
        notify('', ErrorList[400]);
        return true;
    } else {
        return false;
    }   
}


/* var et event pour l'envoie du msg/url */
var btnSendMsg = document.getElementById('sendMsg');
var btnSendUrl = document.getElementById('sendUrl');

btnSendMsg.addEventListener("click", function(){
    if (isNotReadyNotify(userID, userName)){
        return
    }
    msg = document.getElementById("txtMsg").value;
    if (msg != ""){
        makeRequestAndClose(urlConstruct(userID, userPass, msg));
    }
})

btnSendUrl.addEventListener("click", function(){
    if (isNotReadyNotify(userID, userName)){
        return
    }
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        if (tabs[0]) {
            currentTabUrl = tabs[0].url;
            makeRequestAndClose(urlConstruct(userID, userPass, currentTabUrl));
        }
    });
})
